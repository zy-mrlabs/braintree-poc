<?php
require 'braintree_api.php';

header('Content-Type: application/json');

$paymentMethodId = $_POST['paymentMethodNonce'];
$chargeResponse = chargePaymentMethod($paymentMethodId);

$response = [
    'success' => !isset($chargeResponse['errors']),
];

echo json_encode($response);
