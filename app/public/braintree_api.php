<?php

function requestClientToken()
{
    $merchantAccountId = getenv('BRAINTREE_MERCHANT_ACCOUNT_ID');
    $query = '{
        "query": "mutation ExampleClientToken($input: CreateClientTokenInput) {
            createClientToken(input: $input) {
                clientToken
            }
        }",
        "variables": {
            "input": {
                "clientToken": {
                    "merchantAccountId": "' . $merchantAccountId . '"
                }
            }
        }
    }';

    $response = sendGraphQLRequest($query);
    return $response['data']['createClientToken']['clientToken'];
}

function chargePaymentMethod($paymentMethodId)
{
    $query = '{
      "query": "mutation chargePaymentMethod($input: ChargePaymentMethodInput!) {
          chargePaymentMethod(input: $input) {
            transaction {
              id
              status
            }
          }
        }" ,
      "variables": {
        "input": {
          "paymentMethodId": "' . $paymentMethodId . '",
          "transaction": {
            "amount": "11.23"
          }
        }
      }
    }';

    $response = sendGraphQLRequest($query);
    return $response;
}

function sendGraphQLRequest($query)
{
    // https://graphql.braintreepayments.com/guides/making_api_calls/#endpoints
    $endpoint = 'https://payments.sandbox.braintree-api.com/graphql';

    // https://graphql.braintreepayments.com/guides/making_api_calls/#request-requirements
    $publicKey = getenv('BRAINTREE_PUBLIC_KEY');
    $privateKey = getenv('BRAINTREE_PRIVATE_KEY');
    $token = base64_encode($publicKey . ':' . $privateKey);

    $headers = [
        'Authorization: Basic ' . $token,

        // You MUST provide a Braintree-Version header with a date in the format YYYY-MM-DD.
        // We recommend using the date on which you begin integrating with the GraphQL API.
        'Braintree-Version: 2022-01-01',

        // The content type of a query MUST be application/json.
        'Content-Type: application/json',
    ];

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $endpoint);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    // You SHOULD use the POST method for all requests.
    // https://graphql.braintreepayments.com/guides/making_api_calls/#endpoints
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $query);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $output = curl_exec($ch);

    curl_close($ch);

    $object = json_decode($output, true);
    return $object;
}
