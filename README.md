# Braintree POC

## Getting started

```sh
$ cp env-example .env
```

Configure environment variables in .env file

```sh
$ docker-compose up
```
